// Fill out your copyright notice in the Description page of Project Settings.

#include "AISafari.h"
#include "BayesLearn.h"


// Sets default values for this component's properties
UBayesLearn::UBayesLearn()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	bWantsBeginPlay = true;
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UBayesLearn::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UBayesLearn::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );

	// ...
}

void UBayesLearn::addIntWatchVariable(FString name) {data[name] = new intDataHold();}
void UBayesLearn::addBoolWatchVariable(FString name) {data[name] = new boolDataHold();}
float UBayesLearn::queryVar(FString name, int32 in, int32 option) { return data[name]->query(in,option); }

void UBayesLearn::updateVar(FString name, bool inData, bool trueData, int32 in) {
	data[name]->addData(inData, trueData, in); 
}
