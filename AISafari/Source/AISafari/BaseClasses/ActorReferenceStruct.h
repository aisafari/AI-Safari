
#pragma once
#include <map>

//whenever objects are created, throw a pointer to them here
class GlobalStorage{
private:
	struct actorRefStorage{
		AActor** arr;
		int arrayLength;
	};

	std::map<char*, actorRefStorage> actorReference;

	GlobalStorage();
public:
	GlobalStorage* getInstance();
	void setArray(char* key, int length, AActor** _array);
	AActor** getArray(char* key);
	int getArrayLength(char* key);
};


