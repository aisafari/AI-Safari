#pragma once

#include "AutoAgent.h"
#include "GameFramework/Actor.h"
#include <vector>
#include "FlockerBase.generated.h"

// For Profiler
DECLARE_STATS_GROUP(TEXT("Flocker"), STATGROUP_Flocker, STATCAT_Advanced);

//--------FWD Declaration-----------
class AFlockerManagerBase;

UCLASS()
class AISAFARI_API AFlockerBase : public AAutoAgent
{
	GENERATED_BODY()
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "flocking vars")
	bool bHasManager;
	
	AFlockerBase ** flockers;
	std::vector<AActor*> localFlockers;
	std::vector<int> localFlockerID;
	char * ACTORTYPE = "baseFlockers";
	int numFlockers;
	int numOctantsInCentroid;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "flocking vars")
	FVector centroid;
	FVector sepV;
public:	
	// Sets default values for this actor's properties
	AFlockerBase();

	TSubclassOf<AFlockerManagerBase> * manager;

	//Front facing vars
	UPROPERTY(EditAnywhere, Category = "flocking vars")
		float separation;
	UPROPERTY(EditAnywhere, Category = "flocking vars")
		float cohesion;
	UPROPERTY(EditAnywhere, Category = "flocking vars")
		float alignment;
	UPROPERTY(EditAnywhere, Category = "flocking vars")
		float distThreshold;

	float initDistThresh;

	//Getter / Setter ---------------------------
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Flocking")
	virtual FVector getPos();
	UFUNCTION(BlueprintCallable, Category = "Flocking")
	virtual void setPos(FVector _pos);
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Flocking")
	virtual FVector getCentroid();
	UFUNCTION(BlueprintCallable, Category = "Flocking")
	virtual void setCentroid(FVector _centroid);

	//Vector Stuff --------------------------
	UFUNCTION(BlueprintCallable, Category = "Flocking")
	virtual void calcForces(float deltaTime);
	virtual FVector align();
	UFUNCTION(BlueprintCallable, Category = "Flocking")
	virtual FVector calcCentroid();
	UFUNCTION(BlueprintCallable, Category = "Flocking")
	virtual void findFlee();
	UFUNCTION(BlueprintCallable, Category = "Flocking")
	virtual void findLocal();
	UFUNCTION(BlueprintCallable, Category = "Flocking")
	virtual void cullLocal();

	UFUNCTION(BlueprintCallable, Category = "Flocking")
	virtual void localManage();

	UFUNCTION(BlueprintCallable, Category = "Flocking")
	virtual void findLocalOct();
	virtual void nearestNeighborRecurse(OctantBase* _thisOct);

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;
};
