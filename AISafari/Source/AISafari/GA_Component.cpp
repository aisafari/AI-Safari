// Fill out your copyright notice in the Description page of Project Settings.

#include "AISafari.h"
#include "GA_Component.h"


// Sets default values for this component's properties
UGA_Component::UGA_Component()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	bWantsBeginPlay = true;
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UGA_Component::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

void UGA_Component::addToPop(int32 animalsSeen, float fuelUsage, float speed) {
	toEvolve temp;
	temp.setSpeed(speed);
	temp.setFuel(fuelUsage);
	temp.animalsSeen = animalsSeen;
	population.push_back(temp);
}

#include <time.h>

void UGA_Component::calculate() {
	std::vector<toEvolve> nextGen;
	std::vector<toEvolve*> breedPool;

	srand(time(NULL));

	maxAnimalsSeen =  population[0].animalsSeen;
	minAnimalsSeen = population[0].animalsSeen;

	for (auto t : population) {
		if (t.animalsSeen < minAnimalsSeen)
			minAnimalsSeen = t.animalsSeen;
		else if (t.animalsSeen > maxAnimalsSeen)
			maxAnimalsSeen = t.animalsSeen;
	}

	for (auto t: population) {
		int d = (maxAnimalsSeen - minAnimalsSeen);
		float potentialFit = (t.animalsSeen - minAnimalsSeen) / (float)(maxAnimalsSeen - minAnimalsSeen);
		if (d != 0) {
			if (potentialFit < 0.1f) t.fitness = 0;
			else t.fitness = potentialFit;
		}else t.fitness = 0;

		//determine fitness
		for (int k = 0; k < 100 * t.fitness; ++k) {
			breedPool.push_back(&t);
		}
	}

	for (auto t : population) {
		int random = rand() % breedPool.size();
		nextGen.push_back(breedPool[random]->breed(t, random));
	}
	std::swap(population, nextGen);
	
	for (auto t : population) {
		takeResults(t.speed, t.fuelUsage);
		//speed.Add(t.speed);
		//fuelE.Add(t.fuelUsage);
	}
	population.clear();
}

void UGA_Component::takeResults_Implementation(float speed, float fuel){}


// Called every frame
void UGA_Component::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );

	// ...
}

void toEvolve::setSpeed(float in) { speed = in; speedBits = std::bitset<(sizeof(float) * 8)>(in); }
void toEvolve::setFuel(float in) { fuelUsage = in; fuelBits = std::bitset<(sizeof(float) * 8)>(in); }

toEvolve toEvolve::breed(toEvolve other, float ratio) {
	toEvolve child;
	for (int i = 0; i < speedBits.size(); ++i) {
		//breedSpeed
		if (rand() % 10 > 4) { child.speedBits[i] = speedBits[i]; }
		else { child.speedBits[i] = other.speedBits[i]; }
		//breedFuel
		if (rand() % 10 > 4) { child.fuelBits[i] = fuelBits[i]; }
		else { child.fuelBits[i] = other.fuelBits[i]; }
	}
	//mutate speed
	for (int i = 0; i < child.speedBits.size(); ++i)
	{
		if (rand() % 1000 <= 1) { child.speedBits[i] = !child.speedBits[i]; }
	}
	//mutate fuel
	for (int i = 0; i < child.fuelBits.size(); ++i)
	{
		if (rand() % 1000 <= 1) { child.fuelBits[i] = !child.fuelBits[i]; }
	}

	child.speed = child.speedBits.to_ulong();
	child.fuelUsage = child.fuelBits.to_ulong();
	return child;
}

//toEvolve::toEvolve(float inSpeed, float inFuel, int inAnimals)
//{ setSpeed(inSpeed); setFuel(inFuel); animalsSeen = inAnimals;}