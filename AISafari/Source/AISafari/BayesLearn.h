// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <vector>
#include <map>
#include "Components/ActorComponent.h"
#include "BayesLearn.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent), Blueprintable )
class AISAFARI_API UBayesLearn : public UActorComponent
{
	GENERATED_BODY()

public:	


	struct dataHold {
		int total;
		dataHold() { total = 0; };
		virtual float query(int in, int option) { return 0.5f; };
		virtual void addData(bool inData, bool trueValue, int32 in) {};
	};

	struct boolDataHold : public dataHold {
		int sum;
		float ratio;

		boolDataHold() { sum = 0; ratio = 0; total = 0; };

		void addData(bool inData, bool trueValue, int32 in){
			total++;
			//adds to the sum when correct.
			sum += inData & trueValue;
			ratio = sum / (float)total;
		};

		float query(int in, int option) {
			return ratio;
		};
	};

	struct intDataHold : public dataHold {
		std::map<int,int> sumTrue;
		std::map<int, int> sumFalse;
		int totalsTrue = 0;
		int totalsFalse = 0;
		std::map<int,float> ratiosTrue;
		std::map<int, float> ratiosFalse;

		intDataHold() { total = 0; };

		void addData(bool inData, bool trueValue, int32 in) {
			if (sumTrue.count(in) == 0) {
				sumTrue[in] = 0;
			}
			if (sumFalse.count(in) == 0) {
				sumFalse[in] = 0;
			}
			if (inData == trueValue) {
				sumTrue[in] += 1;
			}else {
				sumFalse[in] += 1;
			}
			
			totalsTrue += trueValue;
			totalsFalse += 1-trueValue;

			ratiosTrue[in] = sumTrue.at(in) / (float)totalsTrue;
			ratiosFalse[in] = sumFalse.at(in) / (float)totalsFalse;
		};
		float query(int in, int option) {
			if(option == 0) return ratiosTrue.at(in);
			else return ratiosFalse.at(in);
		};
	};

	// Sets default values for this component's properties
	UBayesLearn();

	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

	UFUNCTION(BlueprintCallable, Category = "Bayes Stuff")
		void addIntWatchVariable(FString name);

	UFUNCTION(BlueprintCallable, Category = "Bayes Stuff")
		void addBoolWatchVariable(FString name);

	UFUNCTION(BlueprintCallable, Category = "Bayes Stuff")
		float queryVar(FString name, int32 in, int32 option);

	UFUNCTION(BlueprintCallable, Category = "Bayes Stuff")
		void updateVar(FString name, bool inData, bool trueData, int32 in);

private:
	std::map<FString, dataHold*> data;
	
};
