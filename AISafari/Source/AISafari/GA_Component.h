// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"

#include <vector>
#include <bitset>

#include "GA_Component.generated.h"

struct toEvolve {
	toEvolve breed(toEvolve other, float ratio);

	/*toEvolve(float inSpeed, float inFuel, int inAnimals);
	toEvolve() {};*/

	void setSpeed(float in);
	void setFuel(float in);

	float fitness;
	float speed;
	float fuelUsage;
	int animalsSeen;

	std::bitset<(sizeof(float) * 8)> speedBits;
	std::bitset<(sizeof(float) * 8)> fuelBits;
};


UCLASS(Blueprintable, BlueprintType, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class AISAFARI_API UGA_Component : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGA_Component();

	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

	int32 generations;

	UFUNCTION(BlueprintCallable, Category = "Genetic Algorithm")
	void addToPop(int32 animalsSeen, float fuelUsage, float speed);
	UFUNCTION(BlueprintCallable, Category = "Genetic Algorithm")
	void calculate();
	
	UFUNCTION(BlueprintNativeEvent, Category = "Genetic Algorithm", meta = (DisplayName = "TakeResults"))
	void takeResults(float speed, float fuelE);

private:
	std::vector<toEvolve> population;
	int maxAnimalsSeen,minAnimalsSeen;
};

